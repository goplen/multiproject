package com.example.fullexampleapp.presentation.View

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    UiTestActivityTest::class
)
class RunAllTest
{
}