package com.example.fullexampleapp.presentation.View

import android.widget.EditText
import android.widget.TextView
import androidx.core.content.res.TypedArrayUtils.getText
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.example.fullexampleapp.R
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class UiTestActivityTest{
    @JvmField
    @Rule
    val menuRule = ActivityTestRule(UiTestActivity::class.java) // Запускает сценарий с выбранной активности

    @Test
    fun testCorrect() {
        // withId - Поиск по id элемента
        // check - действие в котором происходит что либо
        // matches - действие для сравнения данных
        // withHint - сравнение данных которые находятся в хинте со входными парметрами
        // isDisplayed - проверка на наличие на экране
        // perform - выполнение какого либо действия
        // swipe(Left/Down/Right/Up) - свайпы в лево/ в низ/ в право/ наверх
        // withText - сравнение с текстом
        // Espresso.closeSoftKeyboard - закрытие клавиатуры
        // menuRule.activity.findViewById - поиск по id на активности
        // Thread.sleep(1000) - спим секунду
        // RecyclerViewActions

        onView(withId(R.id.test_form_layout)).check(matches(isDisplayed()))
            .check(matches(withHint("Login")))

        onView(withId(R.id.test_form_layout2)).check(matches(isDisplayed()))
            .perform(typeText("Error"))
            .check(matches(withText("Error")))

        Espresso.closeSoftKeyboard()
            onView(withId(R.id.test_form_layout))
            .perform(typeText("Error"))
            .check(matches(withText("Error")))

        Espresso.closeSoftKeyboard()

        onView(withId(R.id.recycler_test_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.recycler_test_layout)).perform(swipeUp())

        val textResult = menuRule.activity.findViewById<EditText>(R.id.test_form_layout).text.toString()

        onView(withId(R.id.test_form_layout)).check(matches(withText(textResult)))
        Thread.sleep(1000)

        onView(withId(R.id.recycler_test_layout)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click())) // Клик по Recycler

        onView(withId(R.id.button_test_layout)).perform(click())
        Thread.sleep(1000)
    }
}