package com.example.fullexampleapp.data.storage

import android.content.Context
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

internal class SaveDataStorageTest {

    val context = mockk<Context>(relaxed = true)
    val repository = mock<ISaveDataRepository>()

    @Test
    fun `should correct save data`() {
        val sharedStorage = SaveDataStorage(context)
        val expected = true
        Mockito.`when`(repository.saveData("token")).thenReturn(expected)
        val actual = sharedStorage.saveData("token")
        assertEquals(expected, actual)
        
    }
}