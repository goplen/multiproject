package com.example.fullexampleapp.domain.usecase

import com.example.fullexampleapp.data.storage.SaveDataStorage
import com.example.fullexampleapp.domain.repository.SaveDataRepository
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.runner.manipulation.Ordering
import org.mockito.Mockito
import org.mockito.kotlin.mock
import org.mockito.kotlin.times


class SaveDataUseCaseTest {
    val repository = mock<SaveDataRepository>()

    @Test
    fun `Should return true equals`() {
        val expected = true
        Mockito.`when`(repository.saveData("token")).thenReturn(expected)

        val actual = repository.saveData("token")
        assertEquals(expected, actual)
        Mockito.verify(repository, Mockito.times(1)).saveData("token")
    }
}