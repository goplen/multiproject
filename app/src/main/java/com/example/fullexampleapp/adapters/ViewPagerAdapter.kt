package com.example.fullexampleapp.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.fullexampleapp.presentation.View.BehaviorFragment.FirstBehaviorFragment

class ViewPagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> FirstBehaviorFragment()
            else -> FirstBehaviorFragment()
        }
    }
}