package com.example.fullexampleapp.networkInterface

import com.example.fullexampleapp.data.networkMapSearch.SearchElements
import retrofit2.http.GET
import retrofit2.http.Path

interface MyMapSearchInterface {
    @GET("api/interpreter?data=[out:json];area[name=Саранск];node[name~{name_query}, i](area);out geom;")
    suspend fun searchDataQuery(@Path("name_query") query: String): SearchElements
}