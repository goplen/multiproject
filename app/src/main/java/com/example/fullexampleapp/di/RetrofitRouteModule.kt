package com.example.fullexampleapp.di

import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.data.networkMapRoute.NetworkMapRoute
import com.example.fullexampleapp.networkInterface.MyMapRouteInterface
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitRouteModule = module {
    fun provideRouteRetrofit(okHttpClient: OkHttpClient): NetworkMapRoute {
        return Retrofit.Builder()
            .baseUrl(Utils.baseRouteUrl())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(NetworkMapRoute::class.java)
    }
    single {
        provideRouteRetrofit(get())
    }
}