package com.example.fullexampleapp.di

import android.content.Context
import com.example.fullexampleapp.common.AuthInterceptor
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.networkInterface.MyInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitModule = module {
    fun provideOkHttp(context: Context): OkHttpClient {
        val logi = HttpLoggingInterceptor()
        logi.setLevel(HttpLoggingInterceptor.Level.BASIC)
        return OkHttpClient.Builder()
            .addInterceptor(logi)
            .addInterceptor(AuthInterceptor(context))
            .build()
    }

    fun provideRetrofit(okHttpClient: OkHttpClient): MyInterface {
        return Retrofit.Builder()
            .baseUrl(Utils.baseUrl())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MyInterface::class.java)
    }

    single {
        provideOkHttp(get())
    }

    single {
        provideRetrofit(get())
    }

}