package com.example.fullexampleapp.di

import com.example.fullexampleapp.data.network.NetworkStorage
import com.example.fullexampleapp.data.networkMapRoute.RouteRepository
import com.example.fullexampleapp.data.repository.SaveDataRepositoryImpl
import com.example.fullexampleapp.data.storage.ISaveDataRepository
import com.example.fullexampleapp.data.storage.SaveDataStorage
import com.example.fullexampleapp.domain.repository.SaveDataRepository
import com.example.fullexampleapp.domain.usecase.SaveDataUseCase
import org.koin.dsl.module
import kotlin.math.sin

val repoModule = module {
    single {
        NetworkStorage(get())
    }

    single {
        RouteRepository(get())
    }

    single<SaveDataRepository> {
        SaveDataRepositoryImpl(get())
    }

    single<ISaveDataRepository> {
        SaveDataStorage(get())
    }

    single {
        SaveDataUseCase(get())
    }
}