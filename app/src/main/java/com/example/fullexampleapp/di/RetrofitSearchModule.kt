package com.example.fullexampleapp.di

import com.bumptech.glide.util.Util
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.networkInterface.MyMapSearchInterface
import com.google.gson.Gson
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val retrofitSearchModule = module {
    fun provideRetrofitSearch(okHttpClient: OkHttpClient): MyMapSearchInterface {
        return Retrofit.Builder()
            .baseUrl(Utils.baseSearchUrl())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MyMapSearchInterface::class.java)
    }

    single {
        provideRetrofitSearch(get())
    }
}