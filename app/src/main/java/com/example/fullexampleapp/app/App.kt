package com.example.fullexampleapp.app

import android.app.Application
import com.example.fullexampleapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(retrofitModule, repoModule, viewModule, retrofitRouteModule, retrofitSearchModule)
        }
    }
}