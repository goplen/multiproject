package com.example.fullexampleapp.data.networkMapSearch

data class SearchElements(val elements: List<ElementsInfo>)
data class ElementsInfo(val lat: Double?, val long: Double?, val tags: TagsElements)
data class TagsElements(val name: String)


sealed class SearchResult{
    data class Success(val data: SearchElements): SearchResult()
    data class Error(val text: String) : SearchResult()
}