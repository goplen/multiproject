package com.example.fullexampleapp.data.networkMapSearch

import com.example.fullexampleapp.networkInterface.MyMapSearchInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException


class NetworkMapSearch(
    private val retrofit: MyMapSearchInterface) {
    suspend fun getRouteInterface(input: String): SearchResult {
        return withContext(Dispatchers.IO) {
            try {
                val result = retrofit.searchDataQuery(input)
                if (result.elements.isEmpty()) {
                    SearchResult.Error("По вашему запросу ничего не найдено, проверьте корректность запроса")
                } else {
                    SearchResult.Success(result)
                }
            } catch (e: Exception) {
                SearchResult.Error(e.localizedMessage.toString())
            }
        }
    }
}