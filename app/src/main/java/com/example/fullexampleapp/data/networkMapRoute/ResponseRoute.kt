package com.example.fullexampleapp.data.networkMapRoute

data class RoutesResponse(val routes: List<RouteGeometry>)
data class RouteGeometry(val geometry: String)

data class ErrorResult(val message: String)

sealed class RouteResult{
    data class Success(val data: RoutesResponse): RouteResult()
    data class Error(val error: String): RouteResult()
}

sealed class Resource<T>(val data: T?=null, val message: String?=null){
    class Success<T>(data: T): Resource<T>(data)
    class Error<T>(message: String?): Resource<T>(message = message)
}