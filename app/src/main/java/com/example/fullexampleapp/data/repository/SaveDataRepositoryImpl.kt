package com.example.fullexampleapp.data.repository

import com.example.fullexampleapp.data.storage.ISaveDataRepository
import com.example.fullexampleapp.domain.repository.SaveDataRepository

class SaveDataRepositoryImpl(private val repository: ISaveDataRepository): SaveDataRepository {
    override fun saveData(token: String): Boolean {
        return repository.saveData(token)
    }
}