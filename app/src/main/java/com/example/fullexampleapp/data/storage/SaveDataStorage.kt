package com.example.fullexampleapp.data.storage

import android.content.Context

class SaveDataStorage(val context: Context): ISaveDataRepository {
    override fun saveData(token: String): Boolean {
        context.getSharedPreferences("app", Context.MODE_PRIVATE).edit().putString("token", token)
            .apply()
        return true
    }
}