package com.example.fullexampleapp.data.network

import com.google.android.gms.maps.model.LatLng
import org.osmdroid.util.GeoPoint


data class movies(val movieId: String,
                  val name: String,
                  val description: String,
                  val age: String,
                  val images: List<String>,
                  val poster: String,
                  val tags: List<tags>)

data class tags(val idTags: String,
                val tagName: String)

data class ErrorServer(val details: String)

sealed class MovieResult() {
    data class Success(val data: List<movies>): MovieResult()
    data class Error(val error: String): MovieResult()
}

sealed class ResultPolyline() {
    data class Success(val data: String)
}


fun List<LatLng>.toGeoPoint(): List<GeoPoint> {
    val list = this.map {
        GeoPoint(
            it.latitude,
            it.longitude
        )
    }
    return list
}