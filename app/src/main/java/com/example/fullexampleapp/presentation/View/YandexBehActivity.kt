package com.example.fullexampleapp.presentation.View

import android.Manifest
import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.fullexampleapp.databinding.ActivityYandexBehBinding
import com.example.fullexampleapp.presentation.View.BehaviorFragment.FirstBehaviorFragment
import com.example.fullexampleapp.presentation.View.Interface.ClickSender
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.RequestPoint
import com.yandex.mapkit.RequestPointType
import com.yandex.mapkit.directions.DirectionsFactory
import com.yandex.mapkit.directions.driving.*
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.layers.ObjectEvent
import com.yandex.mapkit.location.Location
import com.yandex.mapkit.location.LocationListener
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.MapObject
import com.yandex.mapkit.map.MapObjectCollection
import com.yandex.mapkit.map.MapObjectTapListener
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.user_location.UserLocationLayer
import com.yandex.mapkit.user_location.UserLocationObjectListener
import com.yandex.mapkit.user_location.UserLocationView
import com.yandex.runtime.Error

class YandexBehActivity : AppCompatActivity(), UserLocationObjectListener, LocationListener, ClickSender {

    private lateinit var binding: ActivityYandexBehBinding
    private lateinit var currentLocation: Point
    private lateinit var mapView: MapView
    private lateinit var userLocationLayer: UserLocationLayer
    private lateinit var mapRoute: MapObjectCollection
    private lateinit var mapMarker: MapObjectCollection
    private lateinit var fused: FusedLocationProviderClient
    private lateinit var drivingRouter: DrivingRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey("07488be4-c2be-4756-9911-060ba936e370")
        MapKitFactory.initialize(this)

        binding = ActivityYandexBehBinding.inflate(layoutInflater)
        setContentView(binding.root)

        fused = LocationServices.getFusedLocationProviderClient(this)

        mapView = binding.mapYandex
        mapMarker = mapView.map.mapObjects.addCollection()
        mapRoute = mapView.map.mapObjects.addCollection()

        initBehavior()
        getPermission()
    }

    private fun initBehavior() {
        val bottomBehavior = BottomSheetBehavior.from(binding.behaviorYandex.mainBehavior)
        bottomBehavior.peekHeight = 250
        bottomBehavior.maxHeight = 600

        supportFragmentManager.beginTransaction().replace(binding.behaviorYandex.fragmentChoiserBehavior.id, FirstBehaviorFragment())
            .commit()

    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        userLocationLayer = MapKitFactory.getInstance().createUserLocationLayer(mapView.mapWindow)
        userLocationLayer.isVisible = true

        fused.lastLocation.addOnSuccessListener {
            currentLocation = Point(it.latitude, it.longitude)
            movieCameraOnLocation()
        }
    }

    private fun getPermission() {
        Dexter.withContext(this).withPermissions(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        ).withListener(object : MultiplePermissionsListener{
            override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                getLocation()
            }

            override fun onPermissionRationaleShouldBeShown(
                p0: MutableList<PermissionRequest>?,
                p1: PermissionToken?
            ) {

            }
        }).check()
    }

    private fun createDrivingRoute(endPoint: Point) {
        drivingRouter = DirectionsFactory.getInstance().createDrivingRouter()

        val drivingSession = object : DrivingSession.DrivingRouteListener{
            override fun onDrivingRoutes(p0: MutableList<DrivingRoute>) {
                p0.forEach{
                    mapRoute.addPolyline(it.geometry)
                }
            }

            override fun onDrivingRoutesError(p0: Error) {

            }
        }

        drawDrivingRoute(endPoint, drivingSession)
    }

    private fun drawDrivingRoute(endPoint: Point, drivingSession: DrivingSession.DrivingRouteListener) {
        val vehicleOptions = VehicleOptions()
        val drivingOptions = DrivingOptions()
        drivingOptions.avoidTolls = true
        drivingOptions.routesCount = 1

        val requestPoint = ArrayList<RequestPoint>()

        requestPoint.add(
            RequestPoint(
                currentLocation,
                RequestPointType.WAYPOINT,
                null
            )
        )

        requestPoint.add(
            RequestPoint(
                endPoint,
                RequestPointType.WAYPOINT,
                null
            )
        )

        drivingRouter.requestRoutes(requestPoint, drivingOptions, vehicleOptions, drivingSession)
    }

    private fun drawBehavior() {

    }

    private fun setMarkerOnPosition(position: Point) {
        mapMarker.addPlacemark(position).addTapListener(object: MapObjectTapListener{
            override fun onMapObjectTap(p0: MapObject, p1: Point): Boolean {
                Toast.makeText(this@YandexBehActivity, "Тут какой-то текст", Toast.LENGTH_SHORT).show()
                return true
            }
        })
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView.onStart()
    }

    override fun onStop() {
        super.onStop()
        MapKitFactory.getInstance().onStop()
        mapView.onStop()
    }


    private fun movieCameraOnLocation() {
        mapView.map.move(CameraPosition(currentLocation, 15f,0f,0f)
        , Animation(Animation.Type.SMOOTH, 1f), null
        )
    }

    override fun onObjectAdded(p0: UserLocationView) {

    }

    override fun onObjectRemoved(p0: UserLocationView) {

    }

    override fun onObjectUpdated(p0: UserLocationView, p1: ObjectEvent) {

    }

    override fun onLocationUpdated(p0: Location) {
        currentLocation = Point(p0.position.latitude, p0.position.longitude)
        movieCameraOnLocation()
    }

    override fun onLocationStatusUpdated(p0: LocationStatus) {

    }

    override fun click() {
        Toast.makeText(this, "Произошел клик по item", Toast.LENGTH_SHORT).show()
    }
}