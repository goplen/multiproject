package com.example.fullexampleapp.presentation.View

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.common.toGeoPoint
import com.example.fullexampleapp.databinding.ActivityTestOptionBinding
import com.example.fullexampleapp.presentation.ViewModel.TestOptionViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.maps.android.PolyUtil
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.yandex.mapkit.mapview.MapView
import com.yandex.mapkit.user_location.UserLocationLayer
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.XYTileSource
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapController
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay

class TestOptionActivity : AppCompatActivity(), LocationListener {
    private lateinit var binding: ActivityTestOptionBinding
   private lateinit var mapView: org.osmdroid.views.MapView
   private lateinit var currentLocation: MyLocationNewOverlay
   private lateinit var currentCoordinate: GeoPoint
   private lateinit var fused: FusedLocationProviderClient
   private val vm: TestOptionViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTestOptionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mapView = binding.mapTest
        fused = LocationServices.getFusedLocationProviderClient(this)
        initObserver()
        getPermission()

        Configuration.getInstance()
            .load(this, getSharedPreferences("main", Context.MODE_PRIVATE))
        val tilesNew = XYTileSource("main", 10, 20, 256, ".png",
        arrayOf("http://map.madskill.ru/osm/"))
        mapView.setTileSource(tilesNew)
        mapView.controller.setZoom(15.0)

    }

    private fun getPermission() {
        Dexter.withContext(this)
            .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION)
            .withListener(object: MultiplePermissionsListener{
                override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                    getLocation()
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    p1: PermissionToken?
                ) {

                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    private fun getLocation() {
        currentLocation = MyLocationNewOverlay(GpsMyLocationProvider(this), mapView)
        currentLocation.enableMyLocation()
        currentLocation.enableFollowLocation()
        mapView.overlays.add(currentLocation)
        fused.lastLocation.addOnSuccessListener {
            currentCoordinate = GeoPoint(it.latitude, it.longitude)
            vm.getRouteInfo("${it.longitude},${it.latitude};45.1723,54.1850?alternatives=false")
            movieCameraOnLocation()
        }
    }

    private fun drawRoute(poly: String) {
        val route = Polyline(mapView)
        val decodedCoordinate = PolyUtil.decode(poly)
        route.setPoints(decodedCoordinate.toGeoPoint())
        route.isGeodesic = true
        route.width = 5f
        mapView.overlays.add(route)

    }

    private fun movieCameraOnLocation() {
        mapView.controller.animateTo(currentCoordinate)
    }

    private fun initObserver() {
        vm.routeResponse.observe(this) {
            drawRoute(it.routes[0].geometry)
        }
        vm.errorResponse.observe(this) {
            Utils.defaultAlert(this, it)
        }
    }

    override fun onLocationChanged(p0: Location) {
        Log.d("locationChanged", "${p0.latitude} , ${p0.longitude}")
    }
}