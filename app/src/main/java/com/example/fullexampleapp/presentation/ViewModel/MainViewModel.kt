package com.example.fullexampleapp.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fullexampleapp.data.network.MovieResult
import com.example.fullexampleapp.data.network.NetworkStorage
import com.example.fullexampleapp.data.network.movies
import kotlinx.coroutines.launch


class MainViewModel(
    private val networkStorage: NetworkStorage
): ViewModel() {
    val resultData = MutableLiveData<List<movies>>()
    val errorData = MutableLiveData<String>()

    fun getMovies() {
        viewModelScope.launch {
            val result = networkStorage.getMovie()
            when(result) {
                is MovieResult.Success -> {
                    resultData.value = result.data
                }
                is MovieResult.Error -> {
                    errorData.value = result.error
                }
            }
         }
    }
}