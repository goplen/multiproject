package com.example.fullexampleapp.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.fullexampleapp.data.network.NetworkStorage

class OpenStreetViewModel(
    private val networkStorage: NetworkStorage
): ViewModel() {
    val polyLineRoute = MutableLiveData<String>()
    val errorServer = MutableLiveData<String>()

    fun getRoutePolyline(inputPath: String) {
        
    }
}