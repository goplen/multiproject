package com.example.fullexampleapp.presentation.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputBinding
import com.example.fullexampleapp.databinding.ActivityViewPager1Binding

class ViewPager1Activity : AppCompatActivity() {
    private lateinit var binding: ActivityViewPager1Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewPager1Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}