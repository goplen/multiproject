package com.example.fullexampleapp.presentation.View.BehaviorFragment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.fullexampleapp.R
import com.example.fullexampleapp.presentation.View.Interface.ClickSender

class FirstBehaviorFragment: Fragment(R.layout.first_behavior_fragment) {

    private lateinit var click: ClickSender

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val click = context as ClickSender
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}