package com.example.fullexampleapp.presentation.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.fullexampleapp.adapters.ViewPagerAdapterFirst
import com.example.fullexampleapp.common.Utils
import com.example.fullexampleapp.databinding.ActivityViewPagerFirstBinding
import com.example.fullexampleapp.presentation.ViewModel.ViewPagerFirstViewModel
import com.google.android.material.tabs.TabLayoutMediator
import io.paperdb.Paper
import org.koin.androidx.viewmodel.ext.android.viewModel

class ViewPagerFirstActivity : AppCompatActivity() {
    private lateinit var binding: ActivityViewPagerFirstBinding
    private val vm: ViewPagerFirstViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewPagerFirstBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initObserver()

        vm.getMovies()

    }

    private fun initObserver() {
        vm.resultData.observe(this) {
            binding.viewPager2.adapter = ViewPagerAdapterFirst(this, it)
            TabLayoutMediator(binding.tabLayoutBottom, binding.viewPager2){_,_->}.attach()
            Paper.init(this)
            Paper.book().write("movie", it)
        }

        vm.errorData.observe(this){
            Utils.defaultAlert(this, it)
        }
    }

    fun openViewPager1(view: View) {}
}