package com.example.fullexampleapp.presentation.ViewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.fullexampleapp.data.networkMapRoute.Resource
import com.example.fullexampleapp.data.networkMapRoute.RouteRepository
import com.example.fullexampleapp.data.networkMapRoute.RoutesResponse
import kotlinx.coroutines.launch

class RouteViewModel(
    private val routeRepository: RouteRepository
):ViewModel() {
    val successRouteMutableData = MutableLiveData<RoutesResponse>()
    val errorMutableLiveData = MutableLiveData<String?>()


    fun getRoute(inputRoute: String){
            viewModelScope.launch {
            val result = routeRepository.routeDrawer(inputRoute)
            when(result){
                is Resource.Error -> errorMutableLiveData.value = result.message
                is Resource.Success -> {
                    successRouteMutableData.value = result.data
                }
            }
        }
    }

}