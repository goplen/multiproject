package com.example.fullexampleapp.common

import android.content.Context
import com.google.gson.Gson
import okhttp3.*

class AuthInterceptor(context: Context): Interceptor {

    val sharedPreferences = context.getSharedPreferences("main", Context.MODE_PRIVATE) // Получаем sharedPreferences
    private fun requestNewGenerate(chain: Interceptor.Chain, token: String): Response { // Пересоздаем прошлый запрос с новыми параметрами уже
        val request = chain.request().newBuilder()
            .header("Auth", "Bearer $token")
            .build()
        return chain.proceed(request)
    }

    private fun refreshToken(): String {
        val token = sharedPreferences.getString("refresh", null) // Получаем токен
        if (token.isNullOrEmpty()) {
            return "None" // В случае если токен пустой возвращаем None в строке
        } else {

            val bodyRefresh = FormBody.Builder()
                .add("refresh_token", "$token")
                .build()

            val client = OkHttpClient()

            val requestNew = Request.Builder()
                .url(Utils.baseUrl() + "refresh")
                .post(bodyRefresh)
                .build()

            val resultRequest = client.newCall(requestNew).execute()
            if (resultRequest.code == 200) {
                val result = Gson().fromJson(requestNew.body.toString(), authRefresh::class.java)
                sharedPreferences.edit().putString("refresh_token", result.auth_token).apply()
                return result.auth_token
            } else {
                return "None"
            }
        }
    }


    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        if (response.code == 401) {
            response.close()

            val token = refreshToken()

            if (token != "None") {
                return requestNewGenerate(chain, token)
            }
        }
        return response
    }
}