package com.example.fullexampleapp.common

data class authRefresh(val auth_token: String, val refresh_token: String)


sealed class RequestServer<T>(val data: T?=null, val message: String?=null) {
    class Success<T>(data: T): RequestServer<T>(data)
    class Error<T>(message: String?): RequestServer<T>(message = message)
}

data class RecyclerTest(val text:String)